<div {if $isPreview}{$instance->getDataAttributeString()}{/if} class="OPC-TinySnakeGame">
    <canvas id="snakeCanvas_{$instance->getUid()}" width="256" height="256" tabindex="-1">
    <script type="module">
        (function() {
            let tileSize  = {$instance->getProperty('tile-size')};
            let boardSize = {$instance->getProperty('board-size')};
            let canvas = snakeCanvas_{$instance->getUid()};
            canvas.width = canvas.height = tileSize * boardSize;
            let ctx    = canvas.getContext("2d", { alpha: false });
            let dirs   = { Right: [1,0], Left: [-1,0], Down: [0,1], Up: [0,-1] };
            let snake  = [[Math.floor(boardSize / 2), Math.floor(boardSize / 2)]],
                apple = [Math.floor(boardSize / 4), Math.floor(boardSize / 4)],
                dir = [0,0];

            let framedur = 1000 / {$instance->getProperty('fps')};

            canvas.onkeydown = e => {
                dir = dirs[e.key.slice(5)] || dir;
                e.preventDefault();
            };

            setInterval(() => {
                snake.unshift([
                    (snake[0][0] + dir[0] + boardSize) % boardSize,
                    (snake[0][1] + dir[1] + boardSize) % boardSize,
                ]);

                if("" + snake[0] == apple) {
                    do {
                        apple = [
                            Math.floor(Math.random() * boardSize),
                            Math.floor(Math.random() * boardSize),
                        ];
                    } while(snake.some(seg => "" + seg == apple));
                }
                else if(snake.slice(1).some(seg => "" + seg == snake[0])) {
                    snake.splice(1);
                }
                else {
                    snake.pop();
                }

                ctx.fillStyle = "{$instance->getProperty('background-color')}";
                ctx.fillRect(0, 0, canvas.width, canvas.height);
                ctx.fillStyle = "{$instance->getProperty('apple-color')}";
                ctx.fillRect(apple[0] * tileSize, apple[1] * tileSize, tileSize, tileSize);
                ctx.fillStyle = "{$instance->getProperty('snake-color')}";

                snake.forEach(([x,y]) => {
                    ctx.fillRect(x * tileSize, y * tileSize, tileSize, tileSize)
                });
            }, framedur);
        })();
    </script>
</div>