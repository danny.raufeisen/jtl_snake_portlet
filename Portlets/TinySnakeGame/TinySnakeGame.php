<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 */

namespace Plugin\jtl_snake_portlet\Portlets\TinySnakeGame;

use JTL\OPC\InputType;
use JTL\OPC\Portlet;

/**
 * Class TinySnakeGame
 * @package Plugin\jtl_snake_portlet\Portlets\TinySnakeGame
 */
class TinySnakeGame extends Portlet
{
    public function getPropertyDesc(): array
    {
        return [
            'snake-color' => [
                'type' => InputType::COLOR,
                'label' => __('Snake color'),
                'default' => '#0f0',
                'width' => 33,
            ],
            'apple-color' => [
                'type' => InputType::COLOR,
                'label' => __('Apple color'),
                'default' => '#f00',
                'width' => 33,
            ],
            'background-color' => [
                'type' => InputType::COLOR,
                'label' => __('Background color'),
                'default' => '#000',
                'width' => 33,
            ],
            'fps' => [
                'type' => InputType::NUMBER,
                'label' => __('Steps per second'),
                'default' => 8,
                'width' => 33,
            ],
            'tile-size' => [
                'type' => InputType::NUMBER,
                'label' => __('Tile width and height (in pixels)'),
                'default' => 16,
                'width' => 33,
            ],
            'board-size' => [
                'type' => InputType::NUMBER,
                'label' => __('Number of rows and columns'),
                'default' => 16,
                'width' => 33,
            ],
        ];
    }
}